package com.example.hw2

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {
    @Query ("SELECT * FROM User")
    fun allUsers():LiveData<List<User>>

    @Insert
    fun insert(vararg user: User)

    @Delete
    fun delete(user: User)

    @Query("DELETE FROM User")
    fun deleteAll()

    @Update
    fun update(vararg user: User)

}