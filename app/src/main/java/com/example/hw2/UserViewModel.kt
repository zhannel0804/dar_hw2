package com.example.hw2

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class UserViewModel (application: Application) : AndroidViewModel(application) {
    private val db:UserSingleton = UserSingleton.getInstance(application)
    internal val allUsers: LiveData<List<User>> = db.UserDao().allUsers()

    fun insert(user: User) {
        db.UserDao().insert(user)
    }

    fun delete(user: User) {
        db.UserDao().delete(user)
    }
    fun deleteAll() {
        db.UserDao().deleteAll()
    }
    fun update(user: User) {
        db.UserDao().update(user)
    }
}