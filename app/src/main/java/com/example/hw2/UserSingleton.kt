package com.example.hw2

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(User::class), version = 1)
abstract class UserSingleton : RoomDatabase() {
    abstract fun UserDao() : UserDao

    companion object {
        private var INSTANCE: UserSingleton? = null
        fun getInstance(context: Context): UserSingleton {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    UserSingleton::class.java,
                    "userdb")
                    .build()
            }
            return INSTANCE as UserSingleton
        }
    }
}