package com.example.hw2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences.Editor
import android.content.SharedPreferences
import java.text.FieldPosition




class MainActivity : AppCompatActivity() {

    var pref = applicationContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
    var editor = pref.edit()
    private lateinit var UserList: ArrayList<User>
    private lateinit var model: UserViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        model = ViewModelProviders.of(this).get(UserViewModel::class.java)

        val linearLayoutManager = LinearLayoutManager(
            this, RecyclerView.VERTICAL,false)
        recyclerView.layoutManager = linearLayoutManager

        model.allUsers.observe(this, Observer{ users->
            recyclerView.adapter = RecyclerViewAdapter(users) } )

        editor.putBoolean("isClicked", false);
        editor.commit();

        createUserList();
        buildRecyclerView();
        removeItem();
    }

    public fun removeItem(position: Int) {
        UserList.removeAt(position)
        adapter.notifyItemChanged(position)
    }
    public fun buildRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        adapter.setOnItemClickListener(RecyclerViewAdapter.OnItemClickListener() {
            fun onDeleteClick(position: Int) {
                removeItem(position)
            }
        })

    }

}
