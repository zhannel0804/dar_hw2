package com.example.hw2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_view.view.*
import android.widget.ImageView
import android.widget.TextView



class RecyclerViewAdapter(val users: List<User>)
    : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private lateinit var mListener: OnItemClickListener;


    interface OnItemClickListener {
        fun onItemClick(position: Int)

        fun onDeleteClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    class ExampleViewHolder(itemView: View, listener: OnItemClickListener?) : RecyclerView.ViewHolder(itemView) {
        var mDeleteImage: ImageView

        init {
            mDeleteImage = itemView.findViewById(R.id.image_delete)

            itemView.setOnClickListener {
                if (listener != null) {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position)
                    }
                }
            }

            mDeleteImage.setOnClickListener(View.OnClickListener {
                if (listener != null) {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onDeleteClick(position)
                    }
                }
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : RecyclerViewAdapter.ViewHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_view,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
        holder.id.text = users[position].id.toString()
        holder.firstName.text = users[position].firstName
        holder.lastName.text = users[position].lastName
        holder.age.text = users[position].age.toString()
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val id = itemView.tvId
        val firstName = itemView.tvFirstName
        val lastName = itemView.tvLastName
        val age = itemView.tvAge
    }
}